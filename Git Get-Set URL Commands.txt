>> origin

       git remote [-v | --verbose]
   or: git remote add [-t <branch>] [-m <master>] [-f] [--tags | --no-tags] [--mirror=<fetch|push>] <name> <url>
   or: git remote rename <old> <new>
   or: git remote remove <name>
   or: git remote set-head <name> (-a | --auto | -d | --delete | <branch>)
   or: git remote [-v | --verbose] show [-n] <name>
   or: git remote prune [-n | --dry-run] <name>
   or: git remote [-v | --verbose] update [-p | --prune] [(<group> | <remote>)...]
   or: git remote set-branches [--add] <name> <branch>...
   or: git remote get-url [--push] [--all] <name>
   or: git remote set-url [--push] <name> <newurl> [<oldurl>]
   or: git remote set-url --add <name> <newurl>
   or: git remote set-url --delete <name> <url>

    -v, --verbose         be verbose; must be placed before a subcommand


 |=> git remote get-url --all origin
		https://vipinkumar21@bitbucket.org/vipinkumar21/bucket-study.git 
		
 |=> git remote get-url --push origin 
		https://vipinkumar21@bitbucket.org/vipinkumar21/bucket-study.git
		
 |=> git remote set-url --push origin https://vipinkumar21@bitbucket.org/vipinkumar21/testvip.git
      Set URL Done
	  
 |=> git remote set-url --add origin https://vipinkumar21@bitbucket.org/vipinkumar21/testvip.git 
      New URL Added Done 
	  
 |=> git remote get-url --all origin
		https://vipinkumar21@bitbucket.org/vipinkumar21/bucket-study.git
		https://vipinkumar21@bitbucket.org/vipinkumar21/testvip.git

 |=> git remote set-url --delete origin https://vipinkumar21@bitbucket.org/vipinkumar21/bucket-study.git